import calendar

date_input = input("Enter a date (dd/mm/yyyy): ")
day, month, year = map(int, date_input.split('/'))
day_of_week = calendar.weekday(year, month, day)
day_name = calendar.day_name[day_of_week]
print(day_name)
def sort_anagrams(list_of_strings):

    dict = {}

    for word in list_of_strings:
        sorted_word = ''.join(sorted(word))

        if sorted_word not in dict:
            dict[sorted_word] = [word]
        else:
            dict[sorted_word].append(word)

    result = list(dict.values())

    return result

list_of_words = ['deltas', 'retainers', 'desalt', 'pants', 'slated', 'generating', 'ternaries', 'smelters', 'termless', 'salted', 'staled', 'greatening', 'lasted', 'resmelts']
print(sort_anagrams(list_of_words))
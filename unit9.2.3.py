def who_is_missing(file_name):

    with open(file_name, 'r') as file:

        contents = file.read()
        numbers = list(map(int, contents.split(',')))
        numbers.sort()
        for i in range(0,len(numbers)-1, 1):
            if numbers[i]+1 != numbers[i + 1]:
                missing_number = numbers[i]+1

        with open('found.txt', 'w') as found_file:
            found_file.write(str(missing_number))
        return missing_number


missing_number = who_is_missing(r'''C:\Users\roy\Desktop\campusIL\python1\findMe.txt''')
if missing_number:
    print(missing_number)

def check_win(secret_word, old_letters_guessed):
    if all(char in old_letters_guessed for char in secret_word):
        return True
    else:
        return False

def show_hidden_word(secret_word, old_letters_guessed):
    current_word = "_" * len(secret_word)
    list_word = list(current_word)
    for l in old_letters_guessed:
        if l in secret_word:
            for i in range(0,len(current_word)):
                if secret_word[i] == l:
                    list_word[i] = l

    current_word = " ".join(list_word)
    return current_word

def check_valid_input(letter_guessed, old_letters_guessed):

    if len(letter_guessed) > 1:
        for l in letter_guessed:
            if not (ord('a') <= ord(l) <= ord('z') or ord('A') <= ord(l) <= ord('Z')):
                return "E1"

        else:
            return False

    elif not (ord('a') <= ord(letter_guessed) <= ord('z') or ord('A') <= ord(letter_guessed) <= ord('Z')):
        return False

    if letter_guessed.lower() in old_letters_guessed:
        return False

    old_letters_guessed.append(letter_guessed)
    return True

def try_update_letter_guessed(letter_guessed, old_letters_guessed):

    if not check_valid_input(letter_guessed, old_letters_guessed):
        print("X")
        sorted_char_list = sorted(old_letters_guessed)
        result_string = " -> ".join(sorted_char_list)
        print(result_string)
        return False
    else:
        return True


def choose_word(file_path, index):

    count = 0
    words_input_file = open(file_path, "r")
    words = words_input_file.read()
    list_word = words.split(" ")
    index = index % len(list_word)
    count_words_list = [0] * len(list_word)
    previous_words_list = []
    for i in range(0, len(list_word)):

        if list_word[i] not in previous_words_list:
            for j in range(0, len(list_word)):
                if (not i == j) and list_word[i] == list_word[j]:
                    previous_words_list.append(list_word[i])
                    count_words_list[i] += 1

    for i in range(0, len(count_words_list)):
        if count_words_list[i] == 0:
            count += 1


    return [count, list_word[index-1]]


def print_start():
    print("""Welcome to the game Hangman
      _    _                                         
     | |  | |                                        
     | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
     |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
     | |  | | (_| | | | | (_| | | | | | | (_| | | | |
     |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                          __/ |                      
                         |___/
    """)

def print_hangman(num_of_wrongs):
    if num_of_wrongs == 0:
        print("""picture 1:
            x-------x""")
    if num_of_wrongs == 1:
        print("""picture 2:
            x-------x
            |
            |
            |
            |
            |
        """)
    if num_of_wrongs == 2:
        print("""picture 3:
            x-------x
            |       |
            |       0
            |
            |
            |
        """)
    if num_of_wrongs == 3:
        print("""picture 4:
            x-------x
            |       |
            |       0
            |       |
            |
            |
        """)
    if num_of_wrongs == 4:
        print("""picture 5:
            x-------x
            |       |
            |       0
            |      /|/
            |
            |
        """)
    if num_of_wrongs == 5:
        print("""picture 6:
            x-------x
            |       |
            |       0
            |      /|/
            |      / 
            |
        """)
    if num_of_wrongs == 6:
        print("""picture 7:
            x-------x
            |       |
            |       0
            |      /|/
            |      / /
            |
        """)

#path = r'''C:\Users\roy\Desktop\campusIL\python1\words.txt'''
def main():
    num_of_wrongs = 0
    old_letters_guessed = []

    print_start()

    path = input("Enter file path: ")
    index = int(input("Enter index: "))

    secret_word = choose_word(path, index)[1]

    print("Lets Start!!!")
    print_hangman(num_of_wrongs)
    print(show_hidden_word(secret_word,old_letters_guessed))

    while not check_win(secret_word,old_letters_guessed) and num_of_wrongs < 6:
        letter_guessed = input("Guess a letter: ")
        letter_guessed = letter_guessed.lower()
        if try_update_letter_guessed(letter_guessed,old_letters_guessed):
            if not letter_guessed in secret_word:
                num_of_wrongs += 1
                print_hangman(num_of_wrongs)

        print(show_hidden_word(secret_word,old_letters_guessed))

    if num_of_wrongs == 6:
        print("LOSE :(")
    else:
        print("WON!!! :)")




if __name__ == "__main__":
    main()
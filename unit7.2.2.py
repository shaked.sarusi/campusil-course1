def numbers_letters_count(my_str):

    digit_count = 0
    letter_count = 0

    for char in my_str:
        if char.isdigit():
            digit_count += 1
        elif char.isalpha() or char.isspace() or char in '.,?!':
            letter_count += 1

    return [digit_count, letter_count]


my_str = "Python 3.6.3"
result = numbers_letters_count(my_str)
print(result)
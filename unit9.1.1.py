def are_files_equal(file1, file2):

    with open(file1, 'r') as f1, open(file2, 'r') as f2:
        content1 = f1.read()
        content2 = f2.read()
    return content1 == content2



file1 = r'''C:\Users\roy\Desktop\campusIL\python1\vacation.txt'''
file2 = r'''C:\Users\roy\Desktop\campusIL\python1\work.txt'''
result = are_files_equal(file1, file2)
print(result)

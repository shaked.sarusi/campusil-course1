def arrow(my_char, max_length):
    arrow_str = ""
    for i in range(1, max_length+1):
        arrow_str += my_char*i + "\n"
        if i == max_length:
            for j in range(max_length-1, 0, -1):
                arrow_str += my_char*j + "\n"
    return arrow_str



print(arrow('*', 5))

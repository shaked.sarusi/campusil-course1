def sequence_del(my_str):

    result = ""

    for i in range(len(my_str)):
        if i == len(my_str) - 1 or my_str[i] != my_str[i + 1]:
            result += my_str[i]

    return result


my_str1 = "ppyyyyythhhhhooonnnnn"
result1 = sequence_del(my_str1)
print(result1)

my_str2 = "SSSSsssshhhh"
result2 = sequence_del(my_str2)
print(result2)

my_str3 = "Heeyyy yyouuuu!!!"
result3 = sequence_del(my_str3)
print(result3)

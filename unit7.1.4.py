def squared_numbers(start, stop):
    squared_list = []
    while start <= stop:
        squared_list.append(start ** 2)
        start += 1

    return squared_list


print(squared_numbers(4, 8))
print(squared_numbers(-3, 3))

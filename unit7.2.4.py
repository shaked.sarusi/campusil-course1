def seven_boom(end_number):

    result = []
    for num in range(0, end_number + 1):
        if num % 7 == 0 or '7' in str(num):
            result.append('BOOM')
        else:
            result.append(num)

    return result


end_number = 17
result = seven_boom(end_number)
print(result)

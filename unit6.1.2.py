def shift_left(my_list):

    if len(my_list) != 3:
        raise ValueError("Input list must have length 3")

    shifted_list = my_list[1:] + [my_list[0]]

    return shifted_list


print(shift_left([0, 1, 2]))

print(shift_left(['monkey', 2.0, 1]))
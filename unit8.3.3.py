def count_chars(my_str):

    new_str = my_str.replace(" ", "")
    dict = {}
    previous_letters = []
    for i in range(0, len(new_str), 1):
        current_letter = new_str[i]

        if not current_letter in previous_letters:
            previous_letters.append(current_letter)
            count = 0
            for j in range(0, len(new_str), 1):
                if current_letter == new_str[j]:
                    count += 1
            dict[current_letter] = count

    return dict


magic_str = "abra cadabra"
print(count_chars(magic_str))
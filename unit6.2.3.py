def format_list(my_list):

    if len(my_list) % 2 != 0:
        raise ValueError("Input list must have even length")

    # Get the elements in even positions and the last element
    even_elements = my_list[0::2]
    last_element = my_list[-1]

    formatted_string = ', '.join(even_elements) + ', and ' + last_element

    return formatted_string


my_list = ["hydrogen", "helium", "lithium", "beryllium", "boron", "magnesium"]
print(format_list(my_list))

def replace_first_char_with_e(input_str):

    first_char = input_str[0]
    replaced_str = first_char + input_str[1:].replace(first_char, 'e')
    print("Replaced string: {}".format(replaced_str))


user_input = input("Enter a string: ")

replace_first_char_with_e(user_input)
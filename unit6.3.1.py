def are_lists_equal(list1, list2):

    if len(list1) != len(list2):
        return False

    # Convert the lists to sets and compare them
    set1 = set(list1)
    set2 = set(list2)

    # Check if the sets are equal
    if set1 == set2:
        return True
    else:
        return False


list1 = [0.6, 1, 2, 3]
list2 = [3, 2, 0.6, 1]
list3 = [9, 0, 5, 10.5]

print(are_lists_equal(list1, list2))
print(are_lists_equal(list1, list3))
def chocolate_maker(small, big, x):
    if (small * 1 + big * 5) < x:
        return False

    if x % 5 <= small and x - big * 5 <= small + big * 5:
        return True

    return False


print(chocolate_maker(3, 1, 8))

print(chocolate_maker(3, 1, 9))

print(chocolate_maker(3, 2, 10))

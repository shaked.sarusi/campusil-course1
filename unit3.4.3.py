def convert_half_lower_half_upper(input_str):

    length = len(input_str)
    split_index = (length + 1) // 2

    first_half = input_str[:split_index]
    second_half = input_str[split_index:]

    converted_str = first_half.lower() + second_half.upper()

    print("Converted string: {}".format(converted_str))


user_input = input("Enter a string: ")
convert_half_lower_half_upper(user_input)

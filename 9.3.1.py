def my_mp3_playlist(file_path):

    with open(file_path, 'r') as file:
        contents = file.readlines()
        len_longest_song = 0
        longest_song = ""
        num_songs = 0
        performer_freq = {}

        for line in contents:
            song, performer, length, none = line.strip().split(';')
            if int(length.split(':')[0])*60 + int(length.split(':')[1]) > len_longest_song:
                len_longest_song = int(length.split(':')[0])*60 + int(length.split(':')[1])
                longest_song = song

            if performer in performer_freq:
                performer_freq[performer] += 1

            else:
                performer_freq[performer] = 1

            num_songs += 1

        most_frequent_performer = max(performer_freq, key=performer_freq.get)
        return (longest_song, num_songs, most_frequent_performer)


result = my_mp3_playlist(r'''C:\Users\roy\Desktop\campusIL\python1\songs.txt''')
print(result)

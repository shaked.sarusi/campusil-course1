def is_valid_input(letter_guessed):
    if len(letter_guessed) > 1:
        for l in letter_guessed:
            if not (ord('a') <= ord(l) <= ord('z') or ord('A') <= ord(l) <= ord('Z')):
                return False

        else:
            return False

    elif not (ord('a') <= ord(letter_guessed) <= ord('z') or ord('A') <= ord(letter_guessed) <= ord('Z')):
        return False

    return True

def main():
    letter = input("Guess a letter")
    print(is_valid_input(letter))
    
if __name__ == "__main__":
    main()
def read_file_lines(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    return lines

def sort_words(lines):
    words = []
    for line in lines:
        words.extend(line.strip().split())
    sorted_words = sorted(list(set(words)))
    return sorted_words

def reverse_lines(lines):
    reversed_lines = [line[::-1] for line in lines]
    return reversed_lines

def print_last_lines(lines, n):
    last_lines = lines[-n:]
    for line in last_lines:
        print(line.strip())


file_path = input("Enter a file path: ")
lines = read_file_lines(file_path)
task = input("Enter a task (rev, sort, last): ")

if task == "sort":
    sorted_words = sort_words(lines)
    print(sorted_words)

elif task == "rev":
    reversed_lines = reverse_lines(lines)
    for line in reversed_lines:
        print(line.strip())

elif task == "last":
    n = int(input("Enter a number: "))
    print_last_lines(lines, n)

else:
    print("Invalid task. Please enter 'rev', 'sort', or 'last'.")

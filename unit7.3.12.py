def show_hidden_word(secret_word, old_letters_guessed):
    current_word = "_" * len(secret_word)
    list_word = list(current_word)
    for l in old_letters_guessed:
        if l in secret_word:
            for i in range(0,len(current_word)):
                if secret_word[i] == l:
                    list_word[i] = l

    current_word = " ".join(list_word)
    return current_word

def check_win(secret_word, old_letters_guessed):
    if all(char in old_letters_guessed for char in secret_word):
        return True
    else:
        return False

def main():
    secret_word = "mammals"
    old_letters_guessed = ['s', 'p', 'j', 'i', 'm', 'k', 'a', 'l']
    print(show_hidden_word(secret_word, old_letters_guessed))
    print(check_win(secret_word,old_letters_guessed))

if __name__ == "__main__":
        main()
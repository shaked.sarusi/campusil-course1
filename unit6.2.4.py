def extend_list_x(list_x, list_y):

    len_x = len(list_x)
    len_y = len(list_y)

    list_x += [None] * len_y
    for i in range(len_x - 1, -1, -1):
        list_x[i + len_y] = list_x[i]

    for i in range(len_y):
        list_x[i] = list_y[i]

    return list_x


x = [4, 5, 6]
y = [1, 2, 3]
print(extend_list_x(x, y))
def print_products(products):
    print("Products: ", ", ".join(products))

def print_num_of_products(products):
    print("Number of products: ", len(products))

def is_product_in_list(products):
    product = input("Enter product name: ")
    if product in products:
        print(product, "is in the list.")
    else:
        print(product, "is not in the list.")

def print_product_occurrences(products):
    product = input("Enter product name: ")
    count = products.count(product)
    print(product, "appears", count, "time(s) in the list.")

def delete_product(products):
    product = input("Enter product name to delete: ")
    if product in products:
        products.remove(product)
        print(product, "has been deleted from the list.")
    else:
        print(product, "is not in the list.")

def add_product(products):
    product = input("Enter product name to add: ")
    products.append(product)
    print(product, "has been added to the list.")

def print_invalid_products(products):
    invalid_products = [product for product in products if len(product) < 3 or not product.isalpha()]
    if invalid_products:
        print("Invalid products: ", ", ".join(invalid_products))
    else:
        print("No invalid products found.")

def remove_duplicates(products):
    products = list(set(products))
    print("Duplicates removed.")

def main():
    products = input("Enter the list of products (separated by commas without spaces): ").split(",")
    while True:
        print("Select an action:")
        print("1. Print the list of products")
        print("2. Print the number of products in the list")
        print("3. Is the product on the list?")
        print("4. How many times does a certain product appear?")
        print("5. Delete a product from the list")
        print("6. Add a product to the list")
        print("7. Print all invalid products")
        print("8. Remove all duplicates in the list")
        print("9. Exit")
        choice = int(input("Enter your choice (1-9): "))
        if choice == 1:
            print_products(products)
        elif choice == 2:
            print_num_of_products(products)
        elif choice == 3:
            is_product_in_list(products)
        elif choice == 4:
            print_product_occurrences(products)
        elif choice == 5:
            delete_product(products)
        elif choice == 6:
            add_product(products)
        elif choice == 7:
            print_invalid_products(products)
        elif choice == 8:
            remove_duplicates(products)
        elif choice == 9:
            print("Exiting...")
            break
        else:
            print("Invalid choice. Please enter a number between 1 and 9.")

if __name__ == "__main__":
    main()

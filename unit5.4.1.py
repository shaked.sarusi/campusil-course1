def func(num1, num2):
    """Calculate sum of num1 and num2.
    :param num1: first value
    :param num2: second value
    :type num1: int
    :type num2: int
    :return: the result of addition between num1 ant num2
    :rtype: int
    """
    return num1+num2



def main():
    # Call the function func
    help(func)
    print(func(5,3))
if __name__ == "__main__":
    main()
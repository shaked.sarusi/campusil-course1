def check_valid_input(letter_guessed, old_letters_guessed):

    if len(letter_guessed) > 1:
        for l in letter_guessed:
            if not (ord('a') <= ord(l) <= ord('z') or ord('A') <= ord(l) <= ord('Z')):
                return False

        else:
            return False

    elif not (ord('a') <= ord(letter_guessed) <= ord('z') or ord('A') <= ord(letter_guessed) <= ord('Z')):
        return False

    if letter_guessed.lower() in old_letters_guessed:
        return False

    old_letters_guessed.append(letter_guessed)
    return True

def try_update_letter_guessed(letter_guessed, old_letters_guessed):

    if not check_valid_input(letter_guessed, old_letters_guessed):
        print("X")
        sorted_char_list = sorted(old_letters_guessed)
        result_string = " -> ".join(sorted_char_list)
        print(result_string)
        print(False)

def main():
    letter_guessed = input("Guess a letter")
    old_letters_guessed =['a','b','t']
    try_update_letter_guessed(letter_guessed,old_letters_guessed)
    print(old_letters_guessed)
if __name__ == "__main__":
    main()
def my_mp4_playlist(file_path, new_song):

    with open(file_path, 'r') as f:
        lines = f.readlines()

    while len(lines) < 3:
        lines.append('\n')

    lines[2] = f"{new_song};{lines[2].split(';')[1]}\n"

    with open(file_path, 'w') as f:
        f.writelines(lines)

    with open(file_path, 'r') as f:
        print(f.read())


my_mp4_playlist(r'''C:\Users\roy\Desktop\campusIL\python1\songs.txt''', "Python Love Story")

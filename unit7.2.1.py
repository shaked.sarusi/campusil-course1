def is_greater(my_list, n):
    result = [x for x in my_list if x > n]
    return result

my_list = [1, 30, 25, 60, 27, 28]
n = 28
result = is_greater(my_list, n)
print(result)
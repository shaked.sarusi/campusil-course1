def mult_tuple(tuple1, tuple2):
    lst1 = len(tuple1)
    lst2 = len(tuple2)

    tuple3 = (())
    for i in range(0, lst1, 1):
        for j in range(0, lst2 - 1, 1):
            temp = tuple()
            temp = temp + (tuple1[i], tuple2[j])

            if temp not in tuple3:
                tuple3 = tuple3 + (temp, )

            temp = tuple()
            temp += ((tuple2[j], tuple1[i]))

            if temp not in tuple3:
                tuple3 = tuple3 + (temp, )

    return tuple3

first_tuple = (1, 2, 3)
second_tuple = (4, 5, 6)
print(mult_tuple(first_tuple, second_tuple))
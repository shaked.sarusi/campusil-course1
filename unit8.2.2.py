def sort_prices(list_of_tuples):
    # getting length of list of tuples
    lst = len(list_of_tuples)
    for i in range(0, lst):

        for j in range(0, lst - i - 1): 
            if (list_of_tuples[j][1] > list_of_tuples[j + 1][1]):
                temp = list_of_tuples[j]
                list_of_tuples[j] = list_of_tuples[j + 1]
                list_of_tuples[j + 1] = temp
    return list_of_tuples


products = [('milk', '5.5'), ('candy', '2.5'), ('bread', '9.0')]
print(sort_prices(products))

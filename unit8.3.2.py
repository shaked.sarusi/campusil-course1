mariah_carey_dict = {
    'first_name': 'Mariah',
    'last_name': 'Carey',
    'birth_date': '27.03.1970',
    'hobbies': ['Sing', 'Compose', 'Act']
}

user_input = int(input("Enter a number between 1 and 7: "))

if user_input == 1:

    print("Mariah's last name:", mariah_carey_dict['last_name'])

elif user_input == 2:

    birth_date = mariah_carey_dict['birth_date']
    birth_month = birth_date.split('.')[1]
    print("Mariah was born in month:", birth_month)

elif user_input == 3:

    num_hobbies = len(mariah_carey_dict['hobbies'])
    print("Mariah has", num_hobbies, "hobbies")

elif user_input == 4:

    last_hobby = mariah_carey_dict['hobbies'][-1]
    print("Mariah's last hobby:", last_hobby)

elif user_input == 5:

    mariah_carey_dict['hobbies'].append('Cooking')
    print("Added 'Cooking' to Mariah's hobbies")

elif user_input == 6:

    birth_date = mariah_carey_dict['birth_date']
    day, month, year = birth_date.split('.')
    birth_tuple = (day, month, year)
    print("Mariah's birth date:", birth_tuple)

elif user_input == 7:

    birth_year = int(mariah_carey_dict['birth_date'].split('.')[-1])
    age = 2023 - birth_year
    mariah_carey_dict['age'] = age
    print("Mariah's age:", age)
def copy_file_content(source, destination):

    with open(source, 'r') as source_file:
        with open(destination, 'w') as destination_file:
            contents = source_file.read()
            destination_file.write(contents)


copy_file_content(r'''C:\Users\roy\Desktop\campusIL\python1\words.txt''', r'''C:\Users\roy\Desktop\campusIL\python1\work.txt''')
